import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Task3 {

    public static List<Integer> getRandomList(int start, int end, int quantity) {
        List<Integer> list = new ArrayList<>();
        return list = new Random().ints(start, end).limit(quantity).boxed().collect(Collectors.toList());

    }

    public static Integer getMax(List<Integer> list) {
        return list.stream().max(Integer::compare).get();

    }

    public static Integer getMin(List<Integer> list) {
        return list.stream().min(Integer::compare).get();
    }

    public static Integer getAvarage(List<Integer> list) {
        return (int) list.stream().mapToInt(i -> i).average().getAsDouble();
    }

    public static Integer getSum(List<Integer> list) {
        return list.stream().mapToInt(i -> i).sum();
    }

    public static Integer getSum2(List<Integer> list) {
        return list.stream().reduce((x, y) -> x + y).get();
    }

    public static Integer count(List<Integer> list, int x) {
        return (int) list.stream().filter(a -> (a >= x)).count();
    }

    public static void main(String[] args) {
        List<Integer> list = getRandomList(1, 1000, 10);
        list.stream().sorted().forEach(x -> System.out.print(x + " "));
        System.out.println("\nmax " + getMax(list));
        System.out.println("min " + getMin(list));
        System.out.println("average " + getAvarage(list));
        System.out.println("sum " + getSum(list));
        System.out.println("sum " + getSum2(list));
        System.out.println("numbers >500= " + count(list, 500));

    }

}
