import Interfaces.Command;
import Interfaces.Int3Consumer;
import task2.CommandImplement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;


public class Task1_2 {
    public static void main(String[] args) throws IOException {
        //Int3Consumer
        int a = 10;
        int b = 20;
        int c = 30;
        //First lambda returns max value
        Int3Consumer max = (d, e, f) -> IntStream.of(d, e, f).max().getAsInt();
        //Second – average
        Int3Consumer average = (d, e, f) -> (int) IntStream.of(d, e, f).average().getAsDouble();

        System.out.println(max.task1(a, b, c));
        System.out.println(average.task1(a, b, c));

        //Task2
        System.out.println("Input one of the followings command:");
        System.out.println("run1 run2 run3 run4\n");
        List<String> inputs = consoleReader("Input command and argument");
        switch (inputs.get(0)) {
            case "run": {
                Command command1 = a1 -> System.out.println(a1 + " command 1");
                command1.run(inputs.get(1));
                break;
            }
            case "run2": {
                Command command2 = System.out::println;
                command2.run (inputs.get(1));
                break;
            }
            case "run3": {
                Command command3 = new Command() {
                    @Override
                    public void run(String a) {
                        System.out.println(a + " command 3");
                    }
                };
                command3.run(inputs.get(1));
            }
            case "run4": {
                CommandImplement command4 = new CommandImplement();
                command4.run(inputs.get(1));
                break;
            }
            default:
                System.out.println("No such command");

        }
    }

    public static List<String> consoleReader(String text) throws IOException {
        List<String> arguments = new ArrayList<>();
        while (arguments.size() < 2) {
            System.out.println(text);
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input = br.readLine().toLowerCase().trim();

            while (input.contains("  ")) {
                input = input.replaceAll("  ", " ");
            }
            arguments = Arrays.asList(input.split(" "));
        }
        return arguments;
    }

}
