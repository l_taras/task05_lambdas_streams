import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Task4 {

    public static List<String> consoleReader(String text) throws IOException {
        List<String> temp = null;
        List<String> arguments = new ArrayList<String>();
        String input;
        System.out.println(text);
        do {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            input = br.readLine().trim();
            if (input.length() < 1) {
                break;
            }

            while (input.contains("  ")) {
                input = input.replaceAll("  ", " ");
            }
            temp = Arrays.asList(input.split(" "));
            arguments.addAll(temp);
        } while (true);
        return arguments;
    }

    public static void main(String[] args) throws IOException {
        List<String> arr;
        arr = consoleReader("Input text");
//        Number of unique words
        System.out.println("number unique words is " + arr.stream().distinct().count());
//        Sorted list of all unique words
        System.out.println();
        arr.stream().sorted().distinct().forEachOrdered(x -> System.out.print(x + " "));
        //Word count
        System.out.println();
        Map<String, Long> wordCount = arr.stream().collect(groupingBy(Function.identity(), counting()));
        wordCount.forEach((x, y) -> System.out.println(x + " " + y + " "));


    }
}

